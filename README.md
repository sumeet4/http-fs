# http-fs

[![Build](https://gitlab.com/Douman/http-fs/badges/master/build.svg)](https://gitlab.com/Douman/http-fs/pipelines)
[![Crates.io](https://img.shields.io/crates/v/http-fs.svg)](https://crates.io/crates/http-fs)
[![Documentation](https://docs.rs/http-fs/badge.svg)](https://docs.rs/crate/http-fs/)
[![dependency status](https://deps.rs/crate/http-fs/1.0.0/status.svg)](https://deps.rs/crate/http-fs)

## Features

- `hyper` - Enables `hyper` integration.

## Usage

### Hyper

```rust
use http_fs::config::{self, StaticFileConfig};
use http_fs::{StaticFiles};
use futures::Future;

use std::path::Path;

#[derive(Clone)]
pub struct DirectoryConfig;
impl StaticFileConfig for DirectoryConfig {
    type FileService = config::DefaultConfig;
    type DirService = config::DefaultConfig;

    fn handle_directory(&self, _path: &Path) -> bool {
        true
    }
}

fn main() {
    let addr = ([127, 0, 0, 1], 3000).into();
    let static_files = StaticFiles::new(DirectoryConfig);

    let server = hyper::Server::bind(&addr).serve(static_files).map_err(|e| eprintln!("server error: {}", e));

    println!("Listening on http://{}", addr);
    hyper::rt::run(server);
}
```
