use http_fs::config::{self, StaticFileConfig};
use http_fs::{ServeEntry, StaticFiles};

use std::path::Path;

pub struct DirectoryConfig;
impl StaticFileConfig for DirectoryConfig {
    type FileService = config::DefaultConfig;
    type DirService = config::DefaultConfig;

    fn handle_directory(&self, _path: &Path) -> bool {
        true
    }
}

#[test]
fn list_directory() {
    let service = StaticFiles::new(DirectoryConfig);

    let result = service.serve(Path::new(""));

    let (path, dir) = match result {
        ServeEntry::Directory(path, dir) => (path, dir),
        res => panic!("Unexpected result {:?}", res),
    };

    let result = String::from_utf8(service.list_dir(&path, dir).to_vec()).unwrap();

    assert!(result.contains("Cargo.toml"));

    let result = service.serve(Path::new("src"));

    let (path, dir) = match result {
        ServeEntry::Directory(path, dir) => (path, dir),
        res => panic!("Unexpected result {:?}", res),
    };

    let result = String::from_utf8(service.list_dir(&path, dir).to_vec()).unwrap();

    assert!(result.contains("lib.rs"));
}
