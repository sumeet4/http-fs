use http_fs::config::{self, StaticFileConfig};
use http_fs::{StaticFiles};
use futures::Future;

use std::path::Path;

#[derive(Clone)]
pub struct DirectoryConfig;
impl StaticFileConfig for DirectoryConfig {
    type FileService = config::DefaultConfig;
    type DirService = config::DefaultConfig;

    fn handle_directory(&self, _path: &Path) -> bool {
        true
    }
}

fn main() {
    let addr = ([127, 0, 0, 1], 3000).into();
    let static_files = StaticFiles::new(DirectoryConfig);

    let server = hyper::Server::bind(&addr).serve(static_files).map_err(|e| eprintln!("server error: {}", e));

    println!("Listening on http://{}", addr);
    hyper::rt::run(server);
}
